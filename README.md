## TodoList

### 介绍

本示例是一个HarmonyOS应用，基于ArkTS和Web组件实现了自定义待办列表功能。

### 效果预览
![](screenshots/devices/todolist.png)

### 工程目录
```

├──entry/src/main/ets                              
│  ├──entryability  
│  │  └──EntryAbility.ets          // 程序入口
│  ├──pages                                     
│  │  └──components
│  │  │  └──Todos.ets
│  │  └──Index.ets                 // 首页
│  │  └──TodoList.ets                 // TodoList页
│  └──viewmodel
│     └──DataModel.ets                // 日志        
└──entry/src/main/resources        // 应用资源目录
   └──rawfile                     
      └──Index.html                // html页面
```

### 使用说明
自定义待办列表

### 相关权限

不涉及

### 依赖

不涉及

### 约束与限制

1. 本示例仅支持标准系统上运行，支持设备：HarmonyOS。

2. HarmonyOS系统：HarmonyOS NEXT Developer Beta1及以上。

3. DevEco Studio版本：DevEco Studio NEXT Developer Beta1及以上。

4. HarmonyOS SDK版本：HarmonyOS NEXT Developer Beta1 SDK及以上。